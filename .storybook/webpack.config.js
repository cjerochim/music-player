const path = require('path');

module.exports = {
  module: {
    rules: [
      {
        test: /\.scss$/,
        loaders: ["style-loader", "css-loader", "sass-loader"],
        include: path.resolve(__dirname, '../')
      },
      {
        test: /\.svg$/,
        loaders: ['svg-url-loader'],
        include: path.resolve(__dirname, '../')
      },
      {
        test: /\.(eot|ttf|woff|woff2)$/,
        loaders: ['file-loader'],
        include: path.resolve(__dirname, '../')
      }
    ]
  }
}
