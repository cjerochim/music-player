# Music Player

## Intro
Simple music player which allows you to search for tracks by Artist, view albums and preview songs.

Preview the Music Player [here](https://fervent-bhaskara-0ffdef.netlify.com/).

It is a __React / Redux__ application, which follows the "[ducks-modular-redux](https://github.com/erikras/ducks-modular-redux)" proposal.

Integrates with the [iTunes Affiliate API](https://affiliate.itunes.apple.com/resources/documentation/itunes-store-web-service-search-api).

## Table of contents
- [Supported Browsers](#markdown-header-supported-browsers)
- [Supported Features](#markdown-header-supported-features)
	- [Mobile](#markdown-header-mobile)
	- [Desktop](#markdown-header-desktop)
- [Getting Started](#markdown-header-getting-started)
	- [Prerequisites](#markdown-header-prerequisites)
	- [Installing](#markdown-header-installing)
	- [Running the tests](#markdown-header-running-the-tests)
	- [Storybook](#markdown-header-storybook)
- [Deployment](#markdown-header-deployment)
- [Code Style](#markdown-header-code-style)
- [Built With](#markdown-header-built-with)  
- [Author](#markdown-header-author)
- [Acknowledgments](#markdown-header-acknowledgments)


## Supported Browsers
- Chrome - (last 2 versions)
- 
## Supported Features
### Mobile 
- Each song’s title, artist, album and album art should be displayed.
- When we tap a song, a media player should show up at the bottom of the screen and start to play the preview for that song.
- The media player should only show once a song is clicked and should stay on the screen from that point onwards and should be reused for any subsequent song played.
- When a song is being played, you must provide some indicator in the list item that the song is being played.
- You can stop playback if a new search is performed, however the preference is for the song to keep playing.
### Desktop
- Each song’s title, artist, album and album art should be displayed.
- When a song is selected on desktop, the screen should split and on the right hand side, we should see the album art and music controls.
- The selected song’s preview should not play automatically, it should simply display the details and let to user play the song preview if they wish.
- Below the music controls, all the songs in the album are listed and playable.
- The music control only needs to have play and pause however the music should keep playing even if a different song is selected on the left hand side.
- If a song is playing and a different song is selected from the left hand side, the song should keep playing, however the right hand side should display details for the song selected.
- There should be some visual indication on the song playing.
- You can stop playback if a new search is performed, however the preference is for the
song to keep playing.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

You require the following installed on your machine before attempting to run the application.

- __Node__ >= v8.3.0
- __NPM__ >= v4.6.1

To install, click [here](https://nodejs.org/en/)

### Installing

A step by step series of examples that tell you have to get a development env running.

Open your favourite terminal and follow the below steps:

Clone the Music player repository.

```
git clone git@bitbucket.org:cjerochim/music-player.git <project-name>
```

Enter the directory and install all the dependencies

```
cd <project-name>
npm install
```

Start the application

```
npm start
```

The application build and provide you a url to preview within the browser.

## Running the tests

All tests are driven through the following frameworks:

- [Jest](https://facebook.github.io/jest/)
- [Enzyme](https://github.com/airbnb/enzyme)

To run all tests

```
npm test
```

To run all tests in watch mode 

```
npm test:watch
```

## Storybook
Provide a UI Kit of all the key components implemented within the application.

### Development
Open a new terminal window and enter the following command.

*This will bundle and present you with a URL to access the UI Kit within the browser*

```
npm run story
```

### Build
Will generate a package of the UI application ready to be hosted. This will be located in ```/storybook-static``` directory

```
npm run book
```

## Deployment

Prepare the application for production run the following command.

```
npm run build
```

This will build a package which will be located in the ```/dist``` folder. All assets will be optimised ready for hosting. 

## Code Style

### JavaScript
All JavaScript follows the [Airbnb JavaScript Style Guide](https://github.com/airbnb/javascript)

There is a ```.eslintrc``` defined within the project. Code editors such as [VS Code](https://code.visualstudio.com/) will automatically detect this configuration and validate.

You can also run the following command to validate in terminal

```
npm run lint
```

### CSS
To ensure consistency and separation, all CSS follows the [Block, Element, Modifier](http://getbem.com/) (BEM). 

## Built With

* [React](https://github.com/facebook/react) - Component based View library.
* [Redux](https://github.com/reactjs/redux) - Application State Management Library.
* [Ramda](https://github.com/ramda/ramda) - Utility library similar to "underscore" but functional.
* [Parceljs](https://parceljs.org/) - Minimal configuration Web Application Bundler.

## Author

* **Chris Jerochim** - *UI / Development* - [cjerochim](https://bitbucket.org/cjerochim/)