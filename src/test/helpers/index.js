import { compose, map, range, addIndex } from 'ramda';

const mapIndex = addIndex(map);

/**
 * Create Music list for testing
 */
const musicItem = (item, index) => ({
  trackId: index,
  artistId: index,
  collectionId: index,
  trackName: `Track ${index}`,
  artistName: `Artist ${index}`,
  collectionName: `Collection ${index}`,
  trackNumber: index,
  artworkUrl60: 'http://via.placeholder.com/60x60',
});

const generateMusicList = amount =>
  compose(mapIndex(musicItem), range(0))(amount);

export default {
  generateMusicList,
};
