import { createStore, compose } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import middleware from './middleware';
import reducer from './modules/reducer';

export default (initialData) => {
  let composedMiddleware;
  if (process.env.NODE_ENV === 'production') {
    composedMiddleware = compose(middleware);
  } else {
    composedMiddleware = composeWithDevTools(middleware);
  }
  const store = createStore(reducer, initialData, composedMiddleware);
  return store;
};
