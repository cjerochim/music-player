import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

import libraryReducer, {
  LOOKUP_ARTIST_START,
  LOOKUP_ARTIST_ERROR,
  LOOKUP_ARTIST_SUCCESS,
  lookupArtistStart,
  lookupArtistSuccess,
  lookupArtistError,
  libraryLookupArtist,
} from './library';

import uri from '../../uri';

describe('Library Reducer', () => {
  describe('ACTIONS / CONSTANTS', () => {
    it('should create an action to notify look up artist has stated', () => {
      const expectedAction = { type: LOOKUP_ARTIST_START, payload: {} };
      expect(lookupArtistStart()).toEqual(expectedAction);
    });
    it('should create an action to notify look up artist has an error', () => {
      const err = { message: 'Error' };
      const expectedAction = { type: LOOKUP_ARTIST_ERROR, payload: { err } };
      expect(lookupArtistError(err)).toEqual(expectedAction);
    });
    it('should create an action to notify look up artist was successfull', () => {
      const count = 19;
      const tracks = [{ track: 'Track 1' }];
      const expectedAction = { type: LOOKUP_ARTIST_SUCCESS, payload: { count, tracks } };
      expect(lookupArtistSuccess(count, tracks)).toEqual(expectedAction);
    });

    describe('libraryLookupArtist', () => {
      beforeEach(() => {
        const mock = new MockAdapter(axios);
        const response = { resultCount: 10, results: [{ track: '1' }] };
        const pathSuccess = uri.getArtist('jack+johnson');
        const pathError = uri.getArtist('error');
        mock.onGet(pathSuccess).reply(200, response);
        mock.onGet(pathError).networkError();
      });
      it('should create an action to successfully retreive tracks by a search term', (done) => {
        const dispatch = jest.fn();
        const searchTerm = 'jack johnson';
        libraryLookupArtist(searchTerm)(dispatch)
          .then(() => {
            expect(dispatch.mock.calls.length).toBe(2);
            expect(dispatch.mock.calls[0][0].type).toBe(LOOKUP_ARTIST_START);
            expect(dispatch.mock.calls[1][0].type).toBe(LOOKUP_ARTIST_SUCCESS);
            expect(dispatch.mock.calls[1][0].payload).toEqual({ count: 10, tracks: [{ track: '1' }] });
            done();
          })
          .catch((err) => {
            expect(err).toBeUndefined();
            done();
          });
      });
      it('should create an action to show error if request was unsuccessful', (done) => {
        const dispatch = jest.fn();
        const searchTerm = 'error';
        libraryLookupArtist(searchTerm)(dispatch)
          .then(() => {
            expect(dispatch.mock.calls.length).toBe(2);
            expect(dispatch.mock.calls[0][0].type).toBe(LOOKUP_ARTIST_START);
            expect(dispatch.mock.calls[1][0].type).toBe(LOOKUP_ARTIST_ERROR);
            done();
          })
          .catch((err) => {
            expect(err).toBeUndefined();
            done();
          });
      });
    });
  });
  describe('Set Library state', () => {
    it('shoud set lookup artist start state', () => {
      const initialState = {
        isLookupStart: false,
        isLookupError: false,
        isLookupSuccess: false,
      };
      const expectedState = {
        isLookupStart: true,
        isLookupError: false,
        isLookupSuccess: false,
      };
      const action = { type: LOOKUP_ARTIST_START, payload: {} };
      expect(libraryReducer(initialState, action)).toEqual(expectedState);
    });
    it('should set lookup artist success state', () => {
      const initialState = {
        isLookupStart: false,
        isLookupError: false,
        isLookupSuccess: false,
        err: {},
        count: 0,
        tracks: [],
      };
      const expectedState = {
        isLookupStart: false,
        isLookupError: false,
        isLookupSuccess: true,
        err: {},
        count: 10,
        tracks: [{ track: '1' }],
      };
      const action = { type: LOOKUP_ARTIST_SUCCESS, payload: { count: 10, tracks: [{ track: '1' }] } };
      expect(libraryReducer(initialState, action)).toEqual(expectedState);
    });
    it('should set lookup artist error state', () => {
      const initialState = {
        isLookupStart: false,
        isLookupError: false,
        isLookupSuccess: false,
        err: {},
        count: 0,
        tracks: [],
      };
      const expectedState = {
        isLookupStart: false,
        isLookupError: true,
        isLookupSuccess: false,
        err: { message: 'Error' },
        count: 0,
        tracks: [],
      };
      const action = { type: LOOKUP_ARTIST_ERROR, payload: { err: { message: 'Error' } } };
      expect(libraryReducer(initialState, action)).toEqual(expectedState);
    });
  });
});
