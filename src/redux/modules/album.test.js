import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

import albumReducer, {
  LOOKUP_ALBUM_START,
  LOOKUP_ALBUM_ERROR,
  LOOKUP_ALBUM_SUCCESS,
  albumLookup,
  lookupAlbumStart,
  lookupAlbumSuccess,
  lookupAlbumError,
} from './album';

import uri from '../../uri';

describe('Album Reducer', () => {
  describe('ACTIONS / CONSTANTS', () => {
    it('should create an action to notify album look up has started', () => {
      const expectedAction = { type: LOOKUP_ALBUM_START, payload: {} };
      expect(lookupAlbumStart()).toEqual(expectedAction);
    });
    it('should create an action to notify of an error', () => {
      const err = { message: 'This is an error' };
      const expectedAction = { type: LOOKUP_ALBUM_ERROR, payload: { err } };
      expect(lookupAlbumError(err)).toEqual(expectedAction);
    });
    it('should create an action to nofiy album request is success', () => {
      const detail = { track: 'This is a track' };
      const tracks = [{ track: 'Track 1' }, { track: 'Track 2' }];
      const expectedAction = { type: LOOKUP_ALBUM_SUCCESS, payload: { tracks, detail } };
      expect(lookupAlbumSuccess(detail, tracks)).toEqual(expectedAction);
    });
    describe('albumLookup', () => {
      beforeEach(() => {
        const mock = new MockAdapter(axios);
        const response = { results: [{ track: '1' }, { track: '2' }] };
        const pathSuccess = uri.getAlbum(12345);
        const pathErrror = uri.getAlbum('error');
        mock.onGet(pathSuccess).reply(200, response);
        mock.onGet(pathErrror).networkError();
      });
      it('should create an action to successfully make a request to get an album by collectionId', (done) => {
        const dispatch = jest.fn();
        const collectionId = '12345';
        const expectedPayload = { detail: { track: '1' }, tracks: [{ track: '2' }] };
        albumLookup(collectionId)(dispatch)
          .then(() => {
            expect(dispatch.mock.calls.length).toBe(2);
            expect(dispatch.mock.calls[0][0].type).toBe(LOOKUP_ALBUM_START);
            expect(dispatch.mock.calls[1][0].type).toBe(LOOKUP_ALBUM_SUCCESS);
            expect(dispatch.mock.calls[1][0].payload).toEqual(expectedPayload);
            done();
          })
          .catch((err) => {
            expect(err).toBeUndefined();
            done();
          });
      });
      it('should throw an error is there is a problem with the network', (done) => {
        const dispatch = jest.fn();
        const collectionId = 'errror';
        albumLookup(collectionId)(dispatch)
          .then(() => {
            expect(dispatch.mock.calls[0][0].type).toBe(LOOKUP_ALBUM_START);
            expect(dispatch.mock.calls[1][0].type).toBe(LOOKUP_ALBUM_ERROR);
            done();
          })
          .catch((err) => {
            expect(err).toBeUndefined();
            done();
          });
      });
    });
  });
  describe('Set Album state', () => {
    it('should set state of album look up start', () => {
      const initialState = {
        isLookupStart: false,
        isLookupError: false,
        isLookupSuccess: false,
      };
      const expectedState = {
        isLookupStart: true,
        isLookupError: false,
        isLookupSuccess: false,
      };
      const action = { type: LOOKUP_ALBUM_START, payload: {} };
      expect(albumReducer(initialState, action)).toEqual(expectedState);
    });
    it('should set state of album look up success', () => {
      const initialState = {
        isLookupStart: false,
        isLookupError: false,
        isLookupSuccess: false,
        detail: {},
        tracks: [],
        err: {},
      };
      const expectedState = {
        isLookupStart: false,
        isLookupError: false,
        isLookupSuccess: true,
        detail: { track: 'Track 1' },
        tracks: [{ track: 'Track 2' }],
        err: {},
      };
      const action = { type: LOOKUP_ALBUM_SUCCESS, payload: { detail: { track: 'Track 1' }, tracks: [{ track: 'Track 2' }] } };
      expect(albumReducer(initialState, action)).toEqual(expectedState);
    });
    it('shouldset state of album look up error', () => {
      const initialState = {
        isLookupStart: false,
        isLookupError: false,
        isLookupSuccess: false,
        detail: {},
        tracks: [],
        err: {},
      };
      const expectedState = {
        isLookupStart: false,
        isLookupError: true,
        isLookupSuccess: false,
        detail: {},
        tracks: [],
        err: {
          message: 'Error',
        },
      };
      const action = { type: LOOKUP_ALBUM_ERROR, payload: { err: { message: 'Error' } } };
      expect(albumReducer(initialState, action)).toEqual(expectedState);
    });
  });
});
