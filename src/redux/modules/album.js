import axios from 'axios';
import { merge, head, slice } from 'ramda';
import uri from '../../uri';

export const LOOKUP_ALBUM_START = 'app/album/LOOKUP_ALBUM_START';
export const LOOKUP_ALBUM_ERROR = 'app/album/LOOKUP_ALBUM_ERROR';
export const LOOKUP_ALBUM_SUCCESS = 'app/album/LOOKUP_ALBUM_SUCCESS';

const initialState = {
  isLookupStart: false,
  isLookupError: false,
  isLookupSuccess: false,
  err: {},
  detail: {},
  tracks: [],
};

export default (state = initialState, action = {}) => {
  const { type, payload } = action;
  switch (type) {
    case LOOKUP_ALBUM_START:
      return merge(state, {
        isLookupStart: true,
        isLookupError: false,
        isLookupSuccess: false,
      });
    case LOOKUP_ALBUM_SUCCESS:
      return merge(state, {
        isLookupStart: false,
        isLookupError: false,
        isLookupSuccess: true,
        err: {},
        detail: payload.detail,
        tracks: payload.tracks,
      });
    case LOOKUP_ALBUM_ERROR:
      return merge(state, {
        isLookupStart: false,
        isLookupError: true,
        isLookupSuccess: false,
        err: payload.err,
      });
    default:
      return state;
  }
};

export const lookupAlbumStart = () =>
  ({ type: LOOKUP_ALBUM_START, payload: {} });

export const lookupAlbumSuccess = (detail, tracks) =>
  ({ type: LOOKUP_ALBUM_SUCCESS, payload: { detail, tracks } });

export const lookupAlbumError = err =>
  ({ type: LOOKUP_ALBUM_ERROR, payload: { err } });

export const albumLookup = collectionId => (dispatch) => {
  const path = uri.getAlbum(collectionId);
  dispatch(lookupAlbumStart());
  return axios.get(path)
    .then(response => response.data)
    .then(({ results }) => {
      const detail = head(results);
      const tracks = slice(1, results.length)(results);
      dispatch(lookupAlbumSuccess(detail, tracks));
    })
    .catch((err) => {
      dispatch(lookupAlbumError(err));
    });
};
