import { combineReducers } from 'redux';

import app from './app';
import search from './search';
import library from './library';
import album from './album';
import player from './player';
import quote from './quote';

export default combineReducers({
  app,
  search,
  library,
  album,
  player,
  quote,
});
