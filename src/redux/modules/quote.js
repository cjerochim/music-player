import { merge } from 'ramda';
import helpers from '../../helpers';

export const QUOTE_REFRESH = 'app/quote/REFRESH_QUOTE';

const initialState = {
  selected: {},
  collection: [
    // Quotes taken from https://www.brainyquote.com/topics/music
    { text: 'One good thing about music, when it hits you, you feel no pain.', by: 'Bob Marley' },
    { text: 'Music is moonlight in the gloomy night of life.', by: 'Jean Paul' },
    { text: 'Where words fail, music speaks.', by: 'Hans Christian Andersen' },
    { text: 'Without music, life would be a mistake.', by: 'Friedrich Nietzsche' },
    { text: 'If music be the food of live, play on.', by: 'William Shakespeare' },
    { text: 'Music can change the world because it can change people.', by: 'Bono' },
    { text: 'Music is a higher revelation than all wisdom and philosophy.', by: 'Ludwig van Beethoven' },
    { text: 'Life is one grand, sweet song,so start the music.', by: 'Ronald Reagan' },
    { text: 'Music washes away from the soul the dust of everyday life.', by: 'Berthold Auerdbach' },
    { text: 'Music is the movement of sound to reach the soul for the education of its virtue.', by: 'Plato' },
    { text: 'Words make you think a thought. Music makes you feel a feeling. A song makes you feel a thought.', by: 'E. Y. Harbug' },
  ],
};

export default (state = initialState, action = {}) => {
  const { type } = action;
  switch (type) {
    case QUOTE_REFRESH:
      return merge(state, {
        selected: helpers.getNewQuote(state.selected, state.collection),
      });
    default:
      return state;
  }
};

export const refreshQuote = () =>
  ({ type: QUOTE_REFRESH, payload: {} });
