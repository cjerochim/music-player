import { merge } from 'ramda';

export const SEARCH_TERM_UPDATE = 'app/search/SEARCH_TERM_UPDATE';
export const SEARCH_TERM_CLEAR = 'app/search/SEARCH_TERM_CLEAR';

const initialState = {
  searchTerm: '',
};

export default (state = initialState, action = {}) => {
  const { type, payload } = action;
  switch (type) {
    case SEARCH_TERM_UPDATE:
      return merge(state, { searchTerm: payload.searchTerm });
    case SEARCH_TERM_CLEAR:
      return merge(state, { searchTerm: '' });
    default:
      return state;
  }
};

export const searchTermUpdate = searchTerm =>
  ({ type: SEARCH_TERM_UPDATE, payload: { searchTerm } });

export const searchTermClear = () =>
  ({ type: SEARCH_TERM_CLEAR, payload: {} });
