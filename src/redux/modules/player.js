import axios from 'axios';
import { merge, head } from 'ramda';
import uri from '../../uri';

export const PLAYER_ACTIVE = 'app/player/PLAYER_ACTIVE';
export const PLAYER_PLAY_TRACK = 'app/player/PLAYER_PLAY_TRACK';
export const PLAYER_LOAD_TRACK_START = 'app/player/PLAYER_LOAD_TRACK_START';
export const PLAYER_LOAD_TRACK_ERROR = 'app/player/PLAYER_LOAD_TRACK_ERROR';
export const PLAYER_LOAD_TRACK_SUCCESS = 'app/player/PLAYER_LOAD_TRACK_SUCCESS';

const initialState = {
  isLoadTrackStart: false,
  isLoadTrackError: false,
  isLoadTrackSuccess: false,
  err: {},
  track: {},
  isActive: false,
  isPlay: false,
};

export default (state = initialState, action = {}) => {
  const { type, payload } = action;
  switch (type) {
    case PLAYER_LOAD_TRACK_START:
      return merge(state, {
        isLoadTrackStart: true,
        isLoadTrackError: false,
        isLoadTrackSuccess: false,
      });
    case PLAYER_LOAD_TRACK_ERROR:
      return merge(state, {
        isLoadTrackStart: false,
        isLoadTrackError: true,
        isLoadTrackSuccess: false,
        err: payload.err,
      });
    case PLAYER_LOAD_TRACK_SUCCESS:
      return merge(state, {
        isLoadTrackStart: false,
        isLoadTrackError: false,
        isLoadTrackSuccess: true,
        track: payload.track,
        err: {},
      });
    case PLAYER_ACTIVE:
      return merge(state, { isActive: payload.isActive });
    case PLAYER_PLAY_TRACK:
      return merge(state, { isPlay: payload.isPlay });
    default:
      return state;
  }
};

export const playerActive = isActive =>
  ({ type: PLAYER_ACTIVE, payload: { isActive } });

export const playerPlayTrack = isPlay =>
  ({ type: PLAYER_PLAY_TRACK, payload: { isPlay } });

export const loadTrackStart = () =>
  ({ type: PLAYER_LOAD_TRACK_START, payload: {} });

export const loadTrackError = err =>
  ({ type: PLAYER_LOAD_TRACK_ERROR, payload: { err } });

export const loadTrackSuccess = track =>
  ({ type: PLAYER_LOAD_TRACK_SUCCESS, payload: { track } });

export const loadTrack = (trackId, isPlay = false, request = axios) => (dispatch) => {
  const path = uri.getTrack(trackId);
  dispatch(loadTrackStart());
  return request.get(path)
    .then(response => response.data)
    .then(({ results }) => {
      const track = head(results);
      dispatch(loadTrackSuccess(track));
      dispatch(playerPlayTrack(isPlay));
    })
    .catch((err) => {
      dispatch(loadTrackError(err));
    });
};
