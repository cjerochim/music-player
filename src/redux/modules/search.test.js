import searchReducer, {
  SEARCH_TERM_UPDATE,
  SEARCH_TERM_CLEAR,
  searchTermUpdate,
  searchTermClear,
} from './search';


describe('Search Reducer', () => {
  describe('ACTIONS / CONSTANTS', () => {
    it('creates an action to update the search term', () => {
      const searchTerm = 'Jack';
      const expectAction = { type: SEARCH_TERM_UPDATE, payload: { searchTerm } };
      expect(searchTermUpdate(searchTerm)).toEqual(expectAction);
    });
    it('creates an action to submit the selected result', () => {
      const expectAction = { type: SEARCH_TERM_CLEAR, payload: {} };
      expect(searchTermClear()).toEqual(expectAction);
    });
  });
  describe('Set Search State', () => {
    it('should set the search term', () => {
      const initialState = {
        searchTerm: '',
      };
      const exectedState = {
        searchTerm: 'Jack',
      };
      const searchTerm = 'Jack';
      const action = { type: SEARCH_TERM_UPDATE, payload: { searchTerm } };
      expect(searchReducer(initialState, action)).toEqual(exectedState);
    });
    it('should clear the search term', () => {
      const initialState = {
        searchTerm: 'Jack',
      };
      const exectedState = {
        searchTerm: '',
      };
      const action = { type: SEARCH_TERM_CLEAR, payload: { } };
      expect(searchReducer(initialState, action)).toEqual(exectedState);
    });
  });
});
