import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

import playerReducer, {
  PLAYER_ACTIVE,
  PLAYER_PLAY_TRACK,
  PLAYER_LOAD_TRACK_START,
  PLAYER_LOAD_TRACK_ERROR,
  PLAYER_LOAD_TRACK_SUCCESS,
  playerActive,
  playerPlayTrack,
  loadTrackStart,
  loadTrackError,
  loadTrackSuccess,
  loadTrack,
} from './player';

import uri from '../../uri';

describe('Player reducer', () => {
  describe('ACTIONS / CONSTANTS', () => {
    it('should create a player active action', () => {
      const isActive = true;
      const expectAction = { type: PLAYER_ACTIVE, payload: { isActive } };
      expect(playerActive(isActive)).toEqual(expectAction);
    });
    it('should create a player play track action', () => {
      const isPlay = true;
      const expectAction = { type: PLAYER_PLAY_TRACK, payload: { isPlay } };
      expect(playerPlayTrack(isPlay)).toEqual(expectAction);
    });
    it('should create a player load track start action', () => {
      const expectAction = { type: PLAYER_LOAD_TRACK_START, payload: { } };
      expect(loadTrackStart()).toEqual(expectAction);
    });
    it('should create a player load track error action', () => {
      const err = { message: 'Error' };
      const expectAction = { type: PLAYER_LOAD_TRACK_ERROR, payload: { err } };
      expect(loadTrackError(err)).toEqual(expectAction);
    });
    it('should create a player load track success action', () => {
      const track = { track: 'Track 1' };
      const expectAction = { type: PLAYER_LOAD_TRACK_SUCCESS, payload: { track } };
      expect(loadTrackSuccess(track)).toEqual(expectAction);
    });
    describe('loadTrack', () => {
      beforeEach(() => {
        const mock = new MockAdapter(axios);
        const response = { results: [{ track: 'Track 1' }] };
        const pathSuccess = uri.getTrack(13455);
        const pathError = uri.getTrack('error');
        mock.onGet(pathSuccess).reply(200, response);
        mock.onGet(pathError).networkError();
      });
      it('should create an action to successfully retreive a track from a trackId', (done) => {
        const dispatch = jest.fn();
        const trackId = '13455';
        loadTrack(trackId, false, axios)(dispatch)
          .then(() => {
            expect(dispatch.mock.calls.length).toBe(3);
            expect(dispatch.mock.calls[0][0].type).toBe(PLAYER_LOAD_TRACK_START);
            expect(dispatch.mock.calls[1][0].type).toBe(PLAYER_LOAD_TRACK_SUCCESS);
            expect(dispatch.mock.calls[2][0].type).toBe(PLAYER_PLAY_TRACK);
            done();
          })
          .catch((err) => {
            expect(err).toBeUndefined();
            done();
          });
      });
      it('should create an action to show error if request was unsuccessful', (done) => {
        const dispatch = jest.fn();
        const trackId = 'error';
        loadTrack(trackId, false, axios)(dispatch)
          .then(() => {
            expect(dispatch.mock.calls.length).toBe(2);
            expect(dispatch.mock.calls[0][0].type).toBe(PLAYER_LOAD_TRACK_START);
            expect(dispatch.mock.calls[1][0].type).toBe(PLAYER_LOAD_TRACK_ERROR);
            done();
          })
          .catch((err) => {
            expect(err).toBeUndefined();
            done();
          });
      });
    });
  });
  describe('Set Player State', () => {
    it('should set the load track start state', () => {
      const initialState = {
        isLoadTrackStart: false,
        isLoadTrackError: false,
        isLoadTrackSuccess: false,
      };
      const expectedState = {
        isLoadTrackStart: true,
        isLoadTrackError: false,
        isLoadTrackSuccess: false,
      };
      const action = { type: PLAYER_LOAD_TRACK_START, payload: {} };
      expect(playerReducer(initialState, action)).toEqual(expectedState);
    });
    it('should set the load track error state', () => {
      const initialState = {
        isLoadTrackStart: false,
        isLoadTrackError: false,
        isLoadTrackSuccess: false,
      };
      const expectedState = {
        isLoadTrackStart: false,
        isLoadTrackError: true,
        isLoadTrackSuccess: false,
        err: { message: 'Error' },
      };
      const err = { message: 'Error' };
      const action = { type: PLAYER_LOAD_TRACK_ERROR, payload: { err } };
      expect(playerReducer(initialState, action)).toEqual(expectedState);
    });
    it('should set the load track success state', () => {
      const initialState = {
        isLoadTrackStart: false,
        isLoadTrackError: false,
        isLoadTrackSuccess: false,
        err: {},
      };
      const expectedState = {
        isLoadTrackStart: false,
        isLoadTrackError: false,
        isLoadTrackSuccess: true,
        track: { track: 'Track 1' },
        err: {},
      };
      const track = { track: 'Track 1' };
      const action = { type: PLAYER_LOAD_TRACK_SUCCESS, payload: { track } };
      expect(playerReducer(initialState, action)).toEqual(expectedState);
    });
    it('should set the player active state', () => {
      const initialState = {
        isActive: false,
      };
      const expectedState = {
        isActive: true,
      };
      const isActive = true;
      const action = { type: PLAYER_ACTIVE, payload: { isActive } };
      expect(playerReducer(initialState, action)).toEqual(expectedState);
    });
    it('should set the play track state', () => {
      const initialState = {
        isPlay: false,
      };
      const expectedState = {
        isPlay: true,
      };
      const isPlay = true;
      const action = { type: PLAYER_PLAY_TRACK, payload: { isPlay } };
      expect(playerReducer(initialState, action)).toEqual(expectedState);
    });
  });
});
