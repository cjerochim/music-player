import axios from 'axios';
import { merge } from 'ramda';
import helpers from '../../helpers';
import url from '../../uri';

export const LOOKUP_ARTIST_START = 'app/library/LOOKUP_ARTIST_START';
export const LOOKUP_ARTIST_ERROR = 'app/library/LOOKUP_ARTIST_ERROR';
export const LOOKUP_ARTIST_SUCCESS = 'app/library/LOOKUP_ARTIST_SUCCESS';

const initialState = {
  isLookupStart: false,
  isLookupError: false,
  isLookupSuccess: false,
  err: {},
  count: 0,
  tracks: [],
};

export default (state = initialState, action = {}) => {
  const { type, payload } = action;

  switch (type) {
    case LOOKUP_ARTIST_START:
      return merge(state, { isLookupStart: true, isLookupError: false, isLookupSuccess: false });
    case LOOKUP_ARTIST_SUCCESS:
      return merge(state, {
        isLookupStart: false,
        isLookupError: false,
        isLookupSuccess: true,
        err: {},
        count: payload.count,
        tracks: payload.tracks,
      });
    case LOOKUP_ARTIST_ERROR:
      return merge(state, {
        isLookupStart: false,
        isLookupError: true,
        isLookupSuccess: false,
        err: payload.err,
      });
    default:
      return state;
  }
};


export const lookupArtistStart = () =>
  ({ type: LOOKUP_ARTIST_START, payload: {} });

export const lookupArtistSuccess = (count, tracks) =>
  ({ type: LOOKUP_ARTIST_SUCCESS, payload: { tracks, count } });

export const lookupArtistError = err =>
  ({ type: LOOKUP_ARTIST_ERROR, payload: { err } });

export const libraryLookupArtist = (searchTerm, limit = 25) => (dispatch) => {
  const term = helpers.formatStringForRequest(searchTerm);
  const path = url.getArtist(term, limit);
  dispatch(lookupArtistStart());
  return axios.get(path)
    .then(response => response.data)
    .then(({ resultCount, results }) => {
      dispatch(lookupArtistSuccess(resultCount, results));
    })
    .catch((err) => {
      dispatch(lookupArtistError(err));
    });
};
