import pkg from '../../../package.json';

const initialState = {
  version: pkg.version,
};

export default (state = initialState, action = {}) => {
  const { type } = action;
  switch (type) {
    default:
      return state;
  }
};
