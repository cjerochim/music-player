import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withKnobs, text, boolean } from '@storybook/addon-knobs';

import NoResults from './NoResults';

import '../../assets/styles/screen.scss';
import '../../assets/styles/story.scss';

const stories = storiesOf('No Results', module);
stories.addDecorator(withKnobs);
stories.add('Default', () => (
  <NoResults
    isAlbumActive={boolean('Is Album Active', false)}
    refreshQuote={action('Refresh Quote')}
    text={text('Message', 'Where words fail, music speaks.')}
    by={text('Cite', 'Hans Christian Andersen')}
  />
));
