import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classname from 'classname';

import './NoResults.scss';

class NoResults extends Component {
  componentWillMount() {
    const { refreshQuote } = this.props;
    refreshQuote();
  }

  render() {
    const { isAlbumActive, text, by } = this.props;
    const componentState = classname('no-results', { 'no-results--is-album-active': isAlbumActive });
    return (
      <div className={componentState}>
        <blockquote className="no-results__quote">
          <p className="no-results__quote-text">{text}</p>
          <footer className="no-results__quote-cite">— {by}</footer>
        </blockquote>
      </div>
    );
  }
}

NoResults.propTypes = {
  refreshQuote: PropTypes.func.isRequired,
  isAlbumActive: PropTypes.bool,
  text: PropTypes.string,
  by: PropTypes.string,
};

NoResults.defaultProps = {
  text: '',
  by: '',
  isAlbumActive: false,
};

export default NoResults;
