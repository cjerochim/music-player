import React from 'react';
import { shallow } from 'enzyme';

import NoResults from './NoResults';

describe('<NoResults />', () => {
  it('renders correctly', () => {
    const props = {
      quote: {
        text: 'Quote',
        by: 'Person',
      },
      refreshQuote: jest.fn(),
    };
    const component = shallow(<NoResults {...props} />);
    expect(component).toMatchSnapshot();
  });
});
