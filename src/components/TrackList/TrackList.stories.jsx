import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, number, boolean } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';

import helpers from '../../test/helpers';

import TrackList from './TrackList';
import NoResults from '../NoResults/NoResults';

import '../../assets/styles/screen.scss';
import '../../assets/styles/story.scss';

const stories = storiesOf('Track List', module);
stories.addDecorator(withKnobs);

stories.add('Default', () => {
  // const previewImage = 'http://is4.mzstatic.com/image/thumb/Music6/v4/42/a7/97/42a7977e-895c-0bca-d8c5-72c436a98f5a/source/500x500bb.jpg';
  const tracks = helpers.generateMusicList(10);
  return (
    <TrackList
      isPlay={boolean('Is Playing', false)}
      isAlbumList={boolean('Is Album List View', false)}
      activeTrackId={number('Selected Track', 1)}
      onSelect={action('On Track Selection')}
      tracks={tracks}
    >
      <NoResults
        refreshQuote={action('Refresh Quote')}
        text="This is a quote"
        by="This is by someone"
      />
    </TrackList>
  );
});
