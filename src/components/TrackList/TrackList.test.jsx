import React from 'react';
import { shallow } from 'enzyme';
import helpers from '../../test/helpers';
import TrackList from './TrackList';


describe('<Track List />', () => {
  it('renders correctly', () => {
    const onSelect = jest.fn();
    const component = shallow(<TrackList onSelect={onSelect} />);
    expect(component).toMatchSnapshot();
  });

  describe('When artist results are returned', () => {
    it('should generate a list of TrackListItem if there are results', () => {
      const onSelect = jest.fn();
      const tracks = helpers.generateMusicList(10);
      const component = shallow(<TrackList tracks={tracks} onSelect={onSelect} />);
      expect(component.find('.track-list__list li').length).toBe(10);
    });
    it('slhould not generate a list of TrackListItem if there are no results', () => {
      const onSelect = jest.fn();
      const tracks = [];
      const component = shallow(<TrackList tracks={tracks} onSelect={onSelect} />);
      expect(component.find('.track-list__list li').length).toBe(0);
    });
  });

  describe('When a user has slected a track on mobile', () => {
    it('should update the component to a mobile state', () => {
      const onSelect = jest.fn();
      const component = shallow(<TrackList onSelect={onSelect} />);
      expect(component.find('.track-list--album').exists()).toBe(false);
    });
  });
  describe('When a user has selected a track and viewing on a desktop', () => {
    it('should update the component to an album state', () => {
      const onSelect = jest.fn();
      const component = shallow(<TrackList isAlbumList onSelect={onSelect} />);
      expect(component.find('.track-list--album').exists()).toBe(true);
    });
  });
});
