import React from 'react';
import PropTypes from 'prop-types';
import classname from 'classname';
import TrackListItem from '../TrackListItem/TrackListItem';
import './TrackList.scss';

const TrackList = ({
  tracks,
  onSelect,
  activeTrackId,
  isAlbumList,
  isPlay,
  children,
}) => {
  const componentState = classname('track-list', { 'track-list--album': isAlbumList });
  const showNoResults = tracks.length === 0 && !isAlbumList;
  return (
    <div className={componentState}>
      {showNoResults ? children : null}
      <ul className="track-list__list">
        { tracks.map(item => (
          <li key={item.trackId}>
            <TrackListItem
              isAlbumList={isAlbumList}
              isPlay={isPlay}
              isSelected={item.trackId === activeTrackId}
              onSelect={onSelect}
              {...item}
            />
          </li>))}
      </ul>
    </div>
  );
};

TrackList.propTypes = {
  onSelect: PropTypes.func.isRequired,
  children: PropTypes.element,
  tracks: PropTypes.arrayOf(PropTypes.shape({
    trackId: PropTypes.number.isRequired,
    artistId: PropTypes.number.isRequired,
    collectionId: PropTypes.number.isRequired,
    trackName: PropTypes.string.isRequired,
    artistName: PropTypes.string.isRequired,
    collectionName: PropTypes.string.isRequired,
    trackNumber: PropTypes.number.isRequired,
    artwork: PropTypes.string,
  })),
  activeTrackId: PropTypes.number,
  isPlay: PropTypes.bool,
  isAlbumList: PropTypes.bool,
};

TrackList.defaultProps = {
  tracks: [],
  activeTrackId: undefined,
  isPlay: false,
  isAlbumList: false,
  children: null,
};

export default TrackList;
