import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classname from 'classname';


import './Image.scss';

class Image extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isProgress: true,
      isLoaded: false,
      isError: false,
    };

    this.onError = this.onError.bind(this);
    this.onLoad = this.onLoad.bind(this);
  }

  componentWillReceiveProps({ src: nextSrc }) {
    const { src } = this.props;
    if (src !== nextSrc) {
      this.setState({ isProgress: true, isLoaded: false, isError: false });
    }
  }

  onLoad() {
    this.setState({ isLoaded: true, isError: false, isProgress: false });
  }

  onError() {
    this.setState({ isError: true, isLoaded: false, isProgress: false });
  }

  render() {
    const { src } = this.props;
    const { isLoaded, isError, isProgress } = this.state;
    const componentState = classname('image', {
      'image--is-progress': isProgress,
      'image--is-loaded': isLoaded,
      'image--is-error': isError,
    });
    return (
      <span className={componentState}>
        <img
          className="image__image"
          src={src}
          width="100%"
          onLoad={this.onLoad}
          onError={this.onError}
          alt=""
        />
        <span className="image__base" style={{ backgroundImage: `url(${src})` }} />
      </span>
    );
  }
}

Image.propTypes = {
  src: PropTypes.string.isRequired,
};

export default Image;
