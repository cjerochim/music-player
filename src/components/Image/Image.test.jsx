import React from 'react';
import { shallow, mount } from 'enzyme';
import Image from './Image';

describe('<Image />', () => {
  it('renders correctly', () => {
    const src = 'http://www.google.com.au';
    const component = shallow(<Image src={src} />);
    expect(component).toMatchSnapshot();
  });

  it('should have an initial state', () => {
    const src = 'http://www.google.com.au';
    const component = shallow(<Image src={src} />);
    const state = {
      isProgress: true,
      isLoaded: false,
      isError: false,
    };
    expect(component.state()).toEqual(state);
  });

  describe('when an image is loading', () => {
    const src = 'http://www.google.com.au';

    it('should accept an image src attribute', () => {
      const component = mount(<Image src={src} />);
      expect(component.props().src).toEqual(src);
    });

    it('should load a default image if there is an image error', () => {
      const state = {
        isProgress: false,
        isLoaded: false,
        isError: true,
      };
      const component = shallow(<Image src={src} />);
      component.find('.image__image').simulate('error');
      expect(component.state()).toEqual(state);
    });

    it('should display image when loaded', () => {
      const state = {
        isProgress: false,
        isLoaded: true,
        isError: false,
      };
      const component = shallow(<Image src={src} />);
      component.find('.image__image').simulate('load');
      expect(component.find('.image').hasClass('image--is-loaded')).toBe(true);
      expect(component.state()).toEqual(state);
    });

    it('should show a spinner when image is in the process of loading', () => {
      const component = shallow(<Image src={src} />);
      expect(component.find('.image').hasClass('image--is-progress')).toBe(true);
    });
  });
});
