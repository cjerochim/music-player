import React from 'react';
import PropTypes from 'prop-types';
import classname from 'classname';

import Search from '../../containers/SearchContainer';
import TrackList from '../../containers/TrackListContainer';
import Album from '../../containers/AlbumContainer';
import NoResults from '../../containers/NoResultsContainer';
import Version from '../../containers/VersionContainer';


import './App.scss';

const App = ({ isActive }) => {
  const componentState = classname('app', { 'app--is-active': isActive });
  return (
    <section className={componentState}>
      <div className="app__row app__row--primary">
        <Search />
        <TrackList>
          <NoResults />
        </TrackList>
      </div>
      <div className="app__row app__row--secondary">
        <Album />
      </div>
      <Version />
    </section>
  );
};

App.propTypes = {
  isActive: PropTypes.bool.isRequired,
};

export default App;
