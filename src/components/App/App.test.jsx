import React from 'react';
import { shallow } from 'enzyme';

import App from './App';

describe('<App />', () => {
  it('should render correctly', () => {
    const props = {
      isActive: false,
    };
    const component = shallow(<App {...props} />);
    expect(component).toMatchSnapshot();
  });
});
