import React from 'react';
import { shallow } from 'enzyme';
import TrackListButtonAlbum from './TrackListButtonAlbum';

describe('<TrackListButtonAlbum />', () => {
  it('renders correctly', () => {
    const props = {
      isSelected: false,
      isAutoplay: false,
      onSelect: jest.fn(),
      trackNumber: 1,
      trackName: 'Track Name',
      collectionName: 'Collection Name',
    };
    const component = shallow(<TrackListButtonAlbum {...props} />);
    expect(component).toMatchSnapshot();
  });
});
