import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withKnobs, text, boolean, number } from '@storybook/addon-knobs';

import TrackListButtonAlbum from './TrackListButtonAlbum';

import '../../assets/styles/screen.scss';
import '../../assets/styles/story.scss';

const stories = storiesOf('Track List Button Album', module);
stories.addDecorator(withKnobs);
stories.add('Default', () => (
  <div className="story__row">
    <TrackListButtonAlbum
      isSelected={boolean('Is Selected', false)}
      isAutoplay={boolean('Is Autopaly', false)}
      trackNumber={number('Track Number', 1)}
      trackName={text('Track', 'Feelin\' the Same Way')}
      collectionName={text('Album', 'Album Name')}
      artistName={text('Artist', 'Come Away With Me')}
      onSelect={action('On Select')}
    />
  </div>
));
