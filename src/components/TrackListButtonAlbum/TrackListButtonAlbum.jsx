import React from 'react';
import PropTypes from 'prop-types';
import classname from 'classname';
import './TrackListButtonAlbum.scss';

const TrackListButtonAlbum = ({
  isSelected,
  isAutoplay,
  onSelect,
  trackNumber,
  trackName,
  collectionName,
}) => {
  const componentState = classname('track-list-button-album', { 'track-list-button-album--is-selected': isSelected });
  return (
    <button className={componentState} onClick={() => onSelect(isAutoplay)}>
      <span className="track-list-button-album__number">{trackNumber}</span>
      <span className="track-list-button-album__group">
        <span className="track-list-button-album__track">{trackName}</span>
        <span className="track-list-button-album__collection">{collectionName}</span>
      </span>
    </button>
  );
};

TrackListButtonAlbum.propTypes = {
  isSelected: PropTypes.bool.isRequired,
  onSelect: PropTypes.func.isRequired,
  isAutoplay: PropTypes.bool.isRequired,
  trackNumber: PropTypes.number.isRequired,
  trackName: PropTypes.string.isRequired,
  collectionName: PropTypes.string.isRequired,
};

TrackListButtonAlbum.defaultProps = {
};

export default TrackListButtonAlbum;
