import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, object } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';

import Album from './Album';

const stories = storiesOf('Album', module);
stories.addDecorator(withKnobs);

const previewImage = 'http://is4.mzstatic.com/image/thumb/Music6/v4/42/a7/97/42a7977e-895c-0bca-d8c5-72c436a98f5a/source/500x500bb.jpg';

stories.add('Default', () => {
  const tracks = [{
    trackId: 1,
    artistId: 1,
    collectionId: 1,
    trackName: 'Some Track name',
    artistName: 'Some Artist',
    collectionName: 'Some Collection',
    trackNumber: 1,
  },
  {
    trackId: 2,
    artistId: 2,
    collectionId: 2,
    trackName: 'Some Track name',
    artistName: 'Some Artist',
    collectionName: 'Some Collection',
    trackNumber: 2,
  }];

  const player = {
    isPlay: false,
    track: {
      trackName: 'Feelin\' the Same Way',
      collectionName: 'Come Away With Me',
      artistName: 'Norah Jones',
      artworkUrl100: previewImage,
      isPlay: false,
    },
  };

  return (
    <Album
      tracks={object('Track Listing', tracks)}
      player={object('Player', player)}
      onSelect={action('On Track Selected')}
      onTogglePlay={action('On Play Toggle')}
    />
  );
});

