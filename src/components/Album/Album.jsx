import React from 'react';
import PropTypes from 'prop-types';
import Player from '../Player/Player';
import AlbumTrackList from '../TrackList/TrackList';
import helpers from '../../helpers';

import './Album.scss';

const Album = ({
  player: {
    track, track: { trackId },
    isPlay,
  },
  tracks,
  onSelect,
  onTogglePlay,
}) => {
  const sortedTracks = helpers.sortTracksByTrackNumber(tracks);
  return (
    <div className="album">
      <header className="album__header" />
      <div className="album__body">
        <Player {...track} isPlay={isPlay} onTogglePlay={onTogglePlay} />
      </div>
      <footer className="album__footer">
        <AlbumTrackList
          activeTrackId={trackId}
          isPlay={isPlay}
          tracks={sortedTracks}
          onSelect={onSelect}
          isAlbumList
        />
      </footer>
    </div>
  );
};

Album.propTypes = {
  onSelect: PropTypes.func.isRequired,
  onTogglePlay: PropTypes.func.isRequired,
  player: PropTypes.shape({
    isPlay: PropTypes.bool,
    track: PropTypes.shape({
      trackId: PropTypes.number,
      previewUrl: PropTypes.string,
      collectionName: PropTypes.string,
      trackName: PropTypes.string,
      artistName: PropTypes.string,
      artworkUrl100: PropTypes.string,
    }),
  }).isRequired,
  tracks: PropTypes.arrayOf(PropTypes.shape({
    trackId: PropTypes.number.isRequired,
    artistId: PropTypes.number.isRequired,
    collectionId: PropTypes.number.isRequired,
    trackName: PropTypes.string.isRequired,
    artistName: PropTypes.string.isRequired,
    collectionName: PropTypes.string.isRequired,
    trackNumber: PropTypes.number.isRequired,
    artwork: PropTypes.string,
  })),
};

Album.defaultProps = {
  tracks: [],
};

export default Album;
