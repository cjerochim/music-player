import React from 'react';
import { shallow } from 'enzyme';

import Album from './Album';

describe('<Album />', () => {
  it('renders correctly', () => {
    const props = {
      album: {},
      player: { track: { trackId: 123124 } },
      onSelect: jest.fn(),
      onTogglePlay: jest.fn(),
    };
    const component = shallow(<Album {...props} />);
    expect(component).toMatchSnapshot();
  });
});
