import React, { Component } from 'react';
import PropTypes from 'prop-types';
import helpers from '../../helpers';

class Audio extends Component {
  constructor(props) {
    super(props);
    this.onTimeUpdate = this.onTimeUpdate.bind(this);
    this.audioRef = null;
  }

  componentDidMount() {
    this.audioRef.addEventListener('timeupdate', this.onTimeUpdate);
  }

  // Only render view if new props
  shouldComponentUpdate(nextProps) {
    const shouldUpdate = helpers.shouldUpdateCompnentByProps(['src', 'isPlay']);
    return shouldUpdate(nextProps, this.props);
  }

  // Update player on current state
  componentDidUpdate() {
    const { isPlay, onError } = this.props;
    if (isPlay) {
      this.audioRef.play()
        .then(() => {

        })
        .catch(() => {
          // Assume autoplay is disbled cancel play
          onError();
        });
      return;
    }
    this.audioRef.pause();
  }

  componentWillUnmount() {
    this.audioRef.removeEventListener('timeupdate', this.onTimeUpdate);
  }

  onTimeUpdate() {
    const { onDuration } = this.props;
    const percent = Math.round((this.audioRef.currentTime / this.audioRef.duration) * 100);
    const duration = Number.isNaN(percent) ? 0 : percent;
    onDuration(duration);
  }

  render() {
    const { src } = this.props;
    return (
      <audio loop autoPlay src={src} ref={(audio) => { this.audioRef = audio; }} />
    );
  }
}

Audio.propTypes = {
  onError: PropTypes.func.isRequired,
  isPlay: PropTypes.bool.isRequired,
  src: PropTypes.string,
  onDuration: PropTypes.func.isRequired,
};

Audio.defaultProps = {
  src: '',
};

export default Audio;
