import React from 'react';
import { mount } from 'enzyme';

import Audio from './Audio';

describe('<Audio />', () => {
  it('renders correctly', () => {
    const props = {
      onDuration: jest.fn(),
    };
    const component = mount(<Audio isPlay {...props} />);
    expect(component).toMatchSnapshot();
  });
});
