import React from 'react';
import { shallow } from 'enzyme';
import Search from './Search';

describe('<Search />', () => {
  it('renders correctly', () => {
    const props = {
      searchTerm: '',
      onUpdate: jest.fn(),
      onSubmit: jest.fn(),
    };
    const component = shallow(<Search {...props} />);
    expect(component).toMatchSnapshot();
  });

  describe('When a user enters a value into the search field', () => {
    let component;
    let onUpdate;
    let onSubmit;
    beforeEach(() => {
      onUpdate = jest.fn();
      onSubmit = jest.fn();
      component = shallow(<Search onUpdate={onUpdate} onSubmit={onSubmit} searchTerm="Hello" />);
    });

    describe('When a user starts entering a serach term', () => {
      it('should trigger the onUpdate event', () => {
        component.find('.search__input').simulate('change', { target: { value: 'Jack' } });
        expect(onUpdate).toHaveBeenCalledWith('Jack');
      });
    });
    describe('When the user submits search term', () => {
      it('should trigger the onSubmit event', () => {
        component.find('form').simulate('submit', { preventDefault() {} });
        expect(onSubmit).toHaveBeenCalledWith('Hello');
      });
    });
  });
});
