import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './Search.scss';

class Search extends Component {
  constructor(props) {
    super(props);
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChange({ target: { value } }) {
    const { onUpdate } = this.props;
    onUpdate(value);
  }

  onSubmit(e) {
    e.preventDefault();
    const { onSubmit, searchTerm } = this.props;
    onSubmit(searchTerm);
  }

  render() {
    const { searchTerm } = this.props;
    return (
      <div className="search">
        <form onSubmit={this.onSubmit}>
          <label className="search__label" htmlFor="search">Search artist</label>
          <div className="search__group">
            <input
              className="search__input"
              type="text"
              id="search"
              value={searchTerm}
              onChange={this.onChange}
            />
            <button className="search__btn">
              <span className="search__btn-text">search</span>
            </button>
          </div>
        </form>
      </div>
    );
  }
}

Search.propTypes = {
  onUpdate: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  searchTerm: PropTypes.string.isRequired,
};

export default Search;
