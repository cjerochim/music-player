import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';

import '../../assets/styles/screen.scss';

import Search from './Search';

const stories = storiesOf('Search', module);
stories.addDecorator(withKnobs);
stories.add('Default', () => (
  <Search
    searchTerm={text('Search Term', 'Norah')}
    onUpdate={action('On Update')}
    onSubmit={action('On Submit')}
  />
));
