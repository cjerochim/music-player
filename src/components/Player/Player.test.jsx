import React from 'react';
import { shallow } from 'enzyme';
import Player from './Player';

describe('<Player />', () => {
  it('renders correctly', () => {
    const props = {
      isPlay: false,
      onTogglePlay: jest.fn(),
    };
    const component = shallow(<Player {...props} />);
    expect(component).toMatchSnapshot();
  });

  describe('When a user selects a track', () => {
    let component;
    let onTogglePlay;
    let props;
    beforeEach(() => {
      props = {
        trackName: 'Awesome Track',
        collectionName: 'Some Group',
        artistName: 'Artist',
      };
      onTogglePlay = jest.fn();
      component = shallow(<Player {...props} onTogglePlay={onTogglePlay} />);
    });
    it('should display the track name and artist for mobile', () => {
      const expected = `${props.trackName} - ${props.artistName}`;
      expect(component.find('.player__detail-title')
        .at(0).text()).toBe(expected);
    });
    it('should display the track name for desktop', () => {
      expect(component.find('.player__detail-title')
        .at(1).text()).toBe(props.trackName);
    });
    it('Should display the album name', () => {
      expect(component.find('.player__detail-text')
        .at(0).text()).toBe(props.collectionName);
    });
    it('should display a play button', () => {
      expect(component.find('.player__button-icon--play').exists()).toBe(true);
    });
  });
  describe('When a user clicks on the play button', () => {
    let component;
    let onTogglePlay;
    let props;
    beforeEach(() => {
      props = {
        trackName: 'Awesome Track',
        collectionName: 'Some Group',
        artistName: 'Artist',
        isPlay: true,
      };
      onTogglePlay = jest.fn();
      component = shallow(<Player {...props} onTogglePlay={onTogglePlay} />);
    });
    it('should show a pause button', () => {
      expect(component.find('.player__button-icon--pause').exists()).toBe(true);
    });
  });
});
