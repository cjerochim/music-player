import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Image from '../Image/Image';
import Audio from '../Audio/Audio';
import helpers from '../../helpers';

import './Player.scss';

class Player extends Component {
  constructor(props) {
    super(props);
    this.onTogglePlay = this.onTogglePlay.bind(this);
    this.onPlayError = this.onPlayError.bind(this);
    this.onDuration = this.onDuration.bind(this);
    this.state = { duration: 0 };
  }


  onDuration(duration) {
    this.setState({ duration });
  }

  onTogglePlay() {
    const { onTogglePlay, isPlay } = this.props;
    onTogglePlay(!isPlay);
  }
  onPlayError() {
    const { onTogglePlay } = this.props;
    onTogglePlay(false);
  }

  render() {
    const {
      isPlay,
      previewUrl,
      artworkUrl100,
      trackName,
      collectionName,
      artistName,
    } = this.props;

    const { duration } = this.state;
    const albumImage = helpers.getImageWithSize(500, artworkUrl100);
    const textLimitMobile = helpers.textLimitEllipsis(23);
    const textLimitDesktop = helpers.textLimitEllipsis(75);
    return (
      <div className="player">
        <div className="player__duration">
          <div className="player__duration-bar" style={{ width: `${duration}%` }} />
        </div>
        <header className="player__header">
          <div className="player__image">
            <Image src={albumImage} />
          </div>
          <div className="player__detail player__detail--mobile">
            <h3 className="player__detail-title">{textLimitMobile(`${trackName} - ${artistName}`)}</h3>
            <p className="player__detail-text">{textLimitMobile(collectionName)}</p>
          </div>
          <div className="player__detail player__detail--desktop">
            <h3 className="player__detail-title">{textLimitDesktop(trackName)}</h3>
            <p className="player__detail-artist">{textLimitDesktop(artistName)}</p>
            <p className="player__detail-text">{textLimitDesktop(collectionName)}</p>
          </div>
        </header>
        <div className="player__body">
          <div className="player__controls">
            <button className="player__button" onClick={this.onTogglePlay}>
              {isPlay ?
                <span className="player__button-icon player__button-icon--pause">
                  <span className="player__button-text">Pause</span>
                </span>
                :
                null
              }
              {!isPlay ?
                <span className="player__button-icon player__button-icon--play">
                  <span className="player__button-text">Play</span>
                </span>
                :
                null
              }
            </button>
            <Audio
              src={previewUrl}
              isPlay={isPlay}
              onTogglePlay={this.onTogglePlay}
              onError={this.onPlayError}
              onDuration={this.onDuration}
            />
          </div>
        </div>
      </div>
    );
  }
}

Player.propTypes = {
  onTogglePlay: PropTypes.func.isRequired,
  isPlay: PropTypes.bool,
  previewUrl: PropTypes.string,
  collectionName: PropTypes.string,
  trackName: PropTypes.string,
  artistName: PropTypes.string,
  artworkUrl100: PropTypes.string,
};

Player.defaultProps = {
  previewUrl: '',
  collectionName: '',
  trackName: '',
  artistName: '',
  isPlay: false,
  artworkUrl100: '',
};

export default Player;
