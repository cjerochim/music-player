import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, boolean, text } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';
import Player from './Player';

import '../../assets/styles/screen.scss';
import '../../assets/styles/story.scss';

const stories = storiesOf('Player', module);
stories.addDecorator(withKnobs);

stories.add('Default', () => {
  const previewImage = 'http://is4.mzstatic.com/image/thumb/Music6/v4/42/a7/97/42a7977e-895c-0bca-d8c5-72c436a98f5a/source/500x500bb.jpg';
  return (
    <Player
      trackName={text('Track', 'Feelin\' the Same Way')}
      collectionName={text('Album', 'Come Away With Me')}
      artistName={text('Artist', 'Come Away With Me')}
      artworkUrl100={text('Thumbnail', previewImage)}
      isPlay={boolean('Is Playing', false)}
      onTogglePlay={action('Toggle Play')}
    />
  );
});
