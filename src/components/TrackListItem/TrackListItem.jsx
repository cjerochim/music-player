import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classname from 'classname';
import TrackListButton from '../TrackListButton/TrackListButton';
import TrackListButtonAlbum from '../TrackListButtonAlbum/TrackListButtonAlbum';
import './TrackListItem.scss';

class TrackListItem extends Component {
  constructor(props) {
    super(props);
    this.onSelect = this.onSelect.bind(this);
  }

  onSelect(isPlay) {
    const { trackId, onSelect, collectionId } = this.props;
    onSelect(trackId, isPlay, collectionId);
  }

  render() {
    const {
      trackName,
      artistName,
      trackNumber,
      collectionName,
      artworkUrl60,
      isPlay,
      isSelected,
      isAlbumList,
    } = this.props;

    const trackButtonProps = {
      isSelected,
      trackNumber,
      trackName,
      artistName,
      collectionName,
      artworkUrl60,
      onSelect: this.onSelect,
    };

    const componentState = classname('track-list-item', {
      'track-list-item--is-playing': isSelected && isPlay,
      'track-list-item--is-album': isAlbumList,
    });

    return (
      <div className={componentState}>
        <div className="track-list-item__group track-list-item__group--is-mobile">
          { isAlbumList ?
            <TrackListButtonAlbum isAutoplay {...trackButtonProps} />
            :
            <TrackListButton isAutoplay {...trackButtonProps} />
          }
        </div>
        <div className="track-list-item__group track-list-item__group--is-desktop">
          { isAlbumList ?
            <TrackListButtonAlbum isAutoplay={isPlay} {...trackButtonProps} />
            :
            <TrackListButton isAutoplay={isPlay} {...trackButtonProps} />
          }
        </div>
      </div>
    );
  }
}

TrackListItem.propTypes = {
  trackId: PropTypes.number.isRequired,
  trackNumber: PropTypes.number.isRequired,
  collectionId: PropTypes.number.isRequired,
  trackName: PropTypes.string.isRequired,
  artistName: PropTypes.string.isRequired,
  collectionName: PropTypes.string.isRequired,
  onSelect: PropTypes.func.isRequired,
  artworkUrl60: PropTypes.string,
  artistId: PropTypes.number,
  isPlay: PropTypes.bool,
  isSelected: PropTypes.bool,
  isAlbumList: PropTypes.bool,
};

TrackListItem.defaultProps = {
  artworkUrl60: undefined,
  artistId: undefined,
  isPlay: false,
  isSelected: false,
  isAlbumList: false,
};

export default TrackListItem;
