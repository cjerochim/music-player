import React from 'react';
import { shallow } from 'enzyme';

import TrackListItem from '../TrackListItem/TrackListItem';

describe('<TrackListItem />', () => {
  it('renders correctly', () => {
    const trackDetails = {
      trackId: 123212,
      trackNumber: 1,
      artistId: 234234,
      collectionId: 23311234,
      trackName: 'Track Name',
      artistName: 'artist Name',
      collectionName: 'Collection name',
      artwork: 'Artist Album',
      onSelect: jest.fn(),
    };
    const component = shallow(<TrackListItem {...trackDetails} />);
    expect(component).toMatchSnapshot();
  });
});
