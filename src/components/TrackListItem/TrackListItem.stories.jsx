import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withKnobs, text, boolean, number } from '@storybook/addon-knobs';

import TrackListItem from './TrackListItem';

import '../../assets/styles/screen.scss';
import '../../assets/styles/story.scss';

const stories = storiesOf('Track List Item', module);
stories.addDecorator(withKnobs);
const previewImage = 'http://is4.mzstatic.com/image/thumb/Music6/v4/42/a7/97/42a7977e-895c-0bca-d8c5-72c436a98f5a/source/500x500bb.jpg';
stories.add('Default', () => (
  <TrackListItem
    isPlay={boolean('Is Playing', false)}
    isSelected={boolean('Is Selected', false)}
    isAlbumList={boolean('Is Album List View', false)}
    trackId={1}
    collectionId={123}
    trackNumber={number('Track Number', 1)}
    trackName={text('Track', 'Feelin\' the Same Way')}
    collectionName={text('Album', 'Come Away With Me')}
    artistName={text('Artist', 'Norah Jones')}
    artworkUrl60={text('Thumbnail', previewImage)}
    onSelect={action('On Select')}
  />
));
