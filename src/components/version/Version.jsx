import React from 'react';
import PropTypes from 'prop-types';

import './Version.scss';

const Version = ({ version }) => (
  <div className="version">
    {version}
  </div>
);

Version.propTypes = {
  version: PropTypes.string.isRequired,
};

export default Version;
