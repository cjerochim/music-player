import React from 'react';
import { shallow } from 'enzyme';

import Version from './Version';

describe('<Version />', () => {
  it('should render correctly', () => {
    const props = { version: '0.0.0' };
    const component = shallow(<Version {...props} />);
    expect(component).toMatchSnapshot();
  });
});
