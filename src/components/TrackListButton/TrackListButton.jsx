import React from 'react';
import PropTypes from 'prop-types';
import classname from 'classname';
import Image from '../Image/Image';
import helpers from '../../helpers';

import './TrackListButton.scss';

const TrackListButton = ({
  isSelected,
  isAutoplay,
  onSelect,
  trackName,
  artistName,
  collectionName,
  artworkUrl60,
}) => {
  const textLimit = helpers.textLimitEllipsis(50);
  const componentState = classname('track-list-button', { 'track-list-button--is-selected': isSelected });
  return (
    <button className={componentState} onClick={() => onSelect(isAutoplay)}>
      <span className="track-list-button__details">
        <span className="track-list-button__track">{textLimit(trackName)}</span>
        <span className="track-list-button__artist">{textLimit(artistName)}</span>
        <span className="track-list-button__collection">{textLimit(collectionName)}</span>
      </span>
      <span className="track-list-button__artwork"><Image src={artworkUrl60} /></span>
    </button>
  );
};

TrackListButton.propTypes = {
  isSelected: PropTypes.bool.isRequired,
  onSelect: PropTypes.func.isRequired,
  isAutoplay: PropTypes.bool.isRequired,
  trackName: PropTypes.string.isRequired,
  artistName: PropTypes.string.isRequired,
  collectionName: PropTypes.string.isRequired,
  artworkUrl60: PropTypes.string,
};

TrackListButton.defaultProps = {
  artworkUrl60: undefined,
};

export default TrackListButton;
