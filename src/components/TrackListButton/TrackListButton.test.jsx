import React from 'react';
import { shallow } from 'enzyme';

import TrackListButton from './TrackListButton';

describe('<TrackListButton />', () => {
  it('renders correctly', () => {
    const props = {
      isSelected: false,
      isAutoplay: false,
      trackName: 'track Name',
      artistName: 'Artist name',
      collectionName: 'Collection name',
      artworkUrl60: 'http://image',
      onSelect: jest.fn(),
    };
    const component = shallow(<TrackListButton {...props} />);
    expect(component).toMatchSnapshot();
  });
});
