
import uri from './index';

describe('URI', () => {
  describe('getAlbum', () => {
    it('should return the correct url including the collectionID', () => {
      const collectionId = '1234';
      const expectedPath = 'https://itunes.apple.com/lookup?id=1234&entity=song';
      expect(uri.getAlbum(collectionId)).toBe(expectedPath);
    });
  });
  describe('getArtist', () => {
    it('should return the correct url with search term included, with default limit', () => {
      const searchTerm = 'jack+johnson';
      const expectedPath = 'https://itunes.apple.com/search?term=jack+johnson&limit=25&entity=musicTrack';
      expect(uri.getArtist(searchTerm)).toBe(expectedPath);
    });
    it('should return the correct url with search Term included and defined limit', () => {
      const searchTerm = 'jack+johnson';
      const limit = 25;
      const expectedPath = 'https://itunes.apple.com/search?term=jack+johnson&limit=25&entity=musicTrack';
      expect(uri.getArtist(searchTerm, limit)).toBe(expectedPath);
    });
  });
  describe('getTrack', () => {
    it('should return the correct url including the trackId', () => {
      const trackId = '12341';
      const expectedPath = 'https://itunes.apple.com/lookup?id=12341';
      expect(uri.getTrack(trackId)).toBe(expectedPath);
    });
  });
});
