
const getAlbum = collectionId =>
  `https://itunes.apple.com/lookup?id=${collectionId}&entity=song`;

const getArtist = (searchTerm, limit = 25) =>
  `https://itunes.apple.com/search?term=${searchTerm}&limit=${limit}&entity=musicTrack`;

const getTrack = trackId =>
  `https://itunes.apple.com/lookup?id=${trackId}`;

export default {
  getAlbum,
  getArtist,
  getTrack,
};
