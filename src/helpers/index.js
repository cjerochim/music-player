/* eslint no-mixed-operators: ["error", {"allowSamePrecedence": true}] */

import {
  sortBy,
  prop,
  curry,
  split,
  join,
  slice,
  trim,
  append,
  replace,
  last,
  compose,
  dropLast,
  toLower,
  pickAll,
  equals,
  not,
  reject,
  propEq,
} from 'ramda';

const sortTracksByTrackNumber = sortBy(prop('trackNumber'));

const getImageWithSize = curry((size, path) => {
  const basePath = compose(dropLast(1), split('/'))(path);
  const image = compose(replace(/[0-9]{2,3}/g, size), last, split('/'))(path);
  const updatePath = compose(join('/'), append(image))(basePath);
  return updatePath;
});

const textLimitEllipsis = curry((characterLimit, text) => {
  if (typeof text === 'undefined') return '';
  if (characterLimit > text.length) return text;
  const shortenText = compose(trim, slice(0, characterLimit));
  const appendEllipse = compose(join(''), append('...'));
  return compose(appendEllipse, shortenText)(text);
});

const formatStringForRequest = text =>
  compose(join('+'), split(' '), toLower, trim)(text);

const shouldUpdateCompnentByProps = curry((attrs, props1, props2) => {
  const getPropsAttr = compose(pickAll(attrs));
  const props1Attrs = getPropsAttr(props1);
  const props2Attrs = getPropsAttr(props2);
  const isNewUpdate = compose(not, equals(props1Attrs))(props2Attrs);
  return isNewUpdate;
});


const getRandomInt = curry((min, max) =>
  Math.floor(Math.random() * (max - min + 1)) + min);

const getNewIndex = getRandomInt(0);


const getNewQuote = curry((selected = {}, collection) => {
  const otherQuotes = reject(propEq('text', selected.text))(collection);
  const targetIndex = getNewIndex(otherQuotes.length - 1);
  const result = otherQuotes[targetIndex];
  return result;
});

export default {
  getImageWithSize,
  sortTracksByTrackNumber,
  textLimitEllipsis,
  formatStringForRequest,
  shouldUpdateCompnentByProps,
  getNewQuote,
};
