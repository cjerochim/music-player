import { compose, range, forEach } from 'ramda';
import helpers from './index';

describe('Helpers', () => {
  describe('sortTracksByTrackNumber', () => {
    it('should sort numbers by trackNumber', () => {
      const tracks = [{ trackNumber: 10 }, { trackNumber: 1 }];
      const expected = [{ trackNumber: 1 }, { trackNumber: 10 }];
      expect(helpers.sortTracksByTrackNumber(tracks)).toEqual(expected);
    });
  });
  describe('getImageWithSize', () => {
    it('should generate a new size of 500x500', () => {
      const initialPath = 'http://is1.mzstatic.com/image/thumb/Music2/v4/a2/66/32/a2663205-663c-8301-eec7-57937c2d0878/source/100x100bb.jpg';
      const expectedPath = 'http://is1.mzstatic.com/image/thumb/Music2/v4/a2/66/32/a2663205-663c-8301-eec7-57937c2d0878/source/500x500bb.jpg';
      expect(helpers.getImageWithSize(500, initialPath)).toBe(expectedPath);
    });
    it('should generate a new size of 500x500 take 2', () => {
      const initialPath = 'http://is1.mzstatic.com/image/thumb/Music2/v4/a2/66/32/a2663205-663c-8301-eec7-57937c2d0878/source/60x60bb.jpg';
      const expectedPath = 'http://is1.mzstatic.com/image/thumb/Music2/v4/a2/66/32/a2663205-663c-8301-eec7-57937c2d0878/source/500x500bb.jpg';
      expect(helpers.getImageWithSize(500, initialPath)).toBe(expectedPath);
    });
  });
  describe('textLimitEllipsis', () => {
    it('should return an empty string if text is undefined', () => {
      const initialString = undefined;
      const expectedString = '';
      expect(helpers.textLimitEllipsis(18, initialString)).toBe(expectedString);
    });
    it('should limit a string and append an ellipsis', () => {
      const initialString = 'This is a sentence with words.';
      const expectedString = 'This is a sentence...';
      expect(helpers.textLimitEllipsis(18, initialString)).toBe(expectedString);
    });

    it('should not append an ellipsis if the text is less than specified limit', () => {
      const initialString = 'This is a sentence with words.';
      const expectedString = 'This is a sentence with words.';
      expect(helpers.textLimitEllipsis(100, initialString)).toBe(expectedString);
    });
  });
  describe('formatStringForRequest', () => {
    it('should format the string jack johnson to jack-johnson', () => {
      const initialString = 'Jack johnson';
      const expectedString = 'jack+johnson';
      expect(helpers.formatStringForRequest(initialString)).toBe(expectedString);
    });
    it('should remove any white space behind text', () => {
      const initialString = 'Jack johnson      ';
      const expectedString = 'jack+johnson';
      expect(helpers.formatStringForRequest(initialString)).toBe(expectedString);
    });
  });

  describe('shouldUpdateCompnentByProps', () => {
    it('should return true if next props and current props dont match', () => {
      const props = ['trackName', 'isActive'];
      const currentProps = { trackName: 'track 1', isActive: false, collectionId: 1234 };
      const nextProps = { trackName: 'track 1', isActive: true, collectionId: 12355 };
      expect(helpers.shouldUpdateCompnentByProps(props, nextProps, currentProps)).toEqual(true);
    });
    it('should return false if next props and current props do match', () => {
      const props = ['trackName', 'isActive'];
      const currentProps = { trackName: 'track 1', isActive: false, collectionId: 1234 };
      const nextProps = { trackName: 'track 1', isActive: false, collectionId: 12355 };
      expect(helpers.shouldUpdateCompnentByProps(props, nextProps, currentProps)).toEqual(false);
    });
  });

  describe('getNewQuote', () => {
    it('should return a quote', () => {
      const selected = {};
      const collection = [
        { text: 'One good thing about music, when it hits you, you feel no pain.', by: 'Bob Marley' },
        { text: 'Music is moonlight in the gloomy night of life.', by: 'Jean Paul' },
        { text: 'Where words fail, music speaks.', by: 'Hans Christian Andersen' },
        { text: 'Without music, life would be a mistake.', by: 'Friedrich Nietzsche' },
        { text: 'If music be the food of live, play on.', by: 'William Shakespeare' },
        { text: 'Music can change the world because it can change people.', by: 'Bono' },
        { text: 'Music is a higher revelation than all wisdom and philosophy.', by: 'Ludwig van Beethoven' },
        { text: 'Life is one grand, sweet song,so start the music.', by: 'Ronald Reagan' },
        { text: 'Music washes away from the soul the dust of everyday life.', by: 'Berthold Auerdbach' },
        { text: 'Music is the movement of sound to reach the soul for the education of its virtue.', by: 'Plato' },
        { text: 'Words make you think a thought. Music makes you feel a feeling. A song makes you feel a thought.', by: 'E. Y. Harbug' },
      ];
      expect(helpers.getNewQuote(selected, collection)).not.toEqual({});
    });
    it('Should return a different quote to the current selected', () => {
      const quotes = [
        { text: 'This is a cool quote', by: 'Someone' },
        { text: 'This is another cool quote', by: 'Someone else' },
      ];
      expect(helpers.getNewQuote(quotes[0], quotes)).not.toEqual(quotes[0]);
    });
    it('should always return a result', () => {
      const selected = {};
      const collection = [
        { text: 'One good thing about music, when it hits you, you feel no pain.', by: 'Bob Marley' },
        { text: 'Music is moonlight in the gloomy night of life.', by: 'Jean Paul' },
        { text: 'Where words fail, music speaks.', by: 'Hans Christian Andersen' },
        { text: 'Without music, life would be a mistake.', by: 'Friedrich Nietzsche' },
        { text: 'If music be the food of live, play on.', by: 'William Shakespeare' },
        { text: 'Music can change the world because it can change people.', by: 'Bono' },
        { text: 'Music is a higher revelation than all wisdom and philosophy.', by: 'Ludwig van Beethoven' },
        { text: 'Life is one grand, sweet song,so start the music.', by: 'Ronald Reagan' },
        { text: 'Music washes away from the soul the dust of everyday life.', by: 'Berthold Auerdbach' },
        { text: 'Music is the movement of sound to reach the soul for the education of its virtue.', by: 'Plato' },
        { text: 'Words make you think a thought. Music makes you feel a feeling. A song makes you feel a thought.', by: 'E. Y. Harbug' },
      ];
      compose(forEach(() => {
        expect(helpers.getNewQuote(selected, collection)).not.toBe(undefined);
        expect(helpers.getNewQuote(selected, collection)).not.toBe(selected);
      }), range(0))(100);
    });
  });
});
