import React from 'react';
import { shallow } from 'enzyme';
import configureStore from 'redux-mock-store';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

import uri from '../uri';

import AlbumContainer from './AlbumContainer';

import {
  loadTrackStart,
  loadTrackSuccess,
  playerPlayTrack,
} from '../redux/modules/player';

describe('<AlbumContainer />', () => {
  let wrapper;
  let mockStore;
  let store;

  beforeEach(() => {
    mockStore = configureStore();
    const state = {
      player: {},
      album: {},
    };

    store = mockStore(state);
    store.dispatch = jest.fn();
    wrapper = shallow(<AlbumContainer store={store} />);
  });

  it('renders with default props', () => {
    expect(wrapper.props()).toEqual(expect.objectContaining({
      player: {},
      tracks: [],
    }));
  });
  describe('onSelect', () => {
    beforeEach(() => {
      const mock = new MockAdapter(axios);
      const path = uri.getTrack(11111);
      const response = { results: [] };
      mock.onGet(path).reply(200, response);
    });

    it('maps onSelect and dispatches a track to be loaded and set to autoplay', (done) => {
      const dispatch = jest.fn();
      const collectionId = 1234;
      const trackId = 11111;
      const isPlay = true;
      wrapper.props().onSelect(trackId, isPlay, collectionId);
      store.dispatch.mock.calls[0][0](dispatch)
        .then(() => {
          expect(dispatch).toHaveBeenCalledWith(loadTrackStart());
          expect(dispatch).toHaveBeenCalledWith(playerPlayTrack(true));
          expect(dispatch).toHaveBeenCalledWith(loadTrackSuccess(undefined));
          done();
        })
        .catch((err) => {
          expect(err).toBeUndefined();
          done();
        });
    });
  });
  describe('onTogglePlay', () => {
    it('maps onToggle play to dispatch and set Play state', () => {
      const isPlay = false;
      wrapper.props().onTogglePlay(isPlay);
      const action = playerPlayTrack(isPlay);
      expect(store.dispatch).toHaveBeenCalledWith(action);
    });
  });
});
