import { connect } from 'react-redux';
import Album from '../components/Album/Album';

import { playerPlayTrack, loadTrack } from '../redux/modules/player';

const mapStateToProps = ({
  player,
  album: { tracks },
}) => ({
  tracks,
  player,
});

const mapDispatchToProps = dispatch => ({
  onTogglePlay: isPlay => dispatch(playerPlayTrack(isPlay)),
  onSelect: (trackId, isPlay) => {
    dispatch(loadTrack(trackId, isPlay));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Album);
