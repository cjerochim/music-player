import React from 'react';
import { shallow } from 'enzyme';
import configureStore from 'redux-mock-store';
import VersionContainer from './VersionContainer';

const mockStore = configureStore();

describe('<VersionContainer />', () => {
  let wrapper;
  let store;
  beforeEach(() => {
    const state = {
      app: { version: '1.0.0' },
    };
    store = mockStore(state);
    store.dispatch = jest.fn();
    wrapper = shallow(<VersionContainer store={store} />);
  });

  it('should render default props', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
