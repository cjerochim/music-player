import { connect } from 'react-redux';
import TrackList from '../components/TrackList/TrackList';

import { albumLookup } from '../redux/modules/album';
import { loadTrack, playerActive } from '../redux/modules/player';

const mapStateToProps = ({
  player: { isPlay, track: { trackId }, isBuffering },
  library: { tracks },
}) =>
  ({
    isBuffering,
    tracks,
    activeTrackId: trackId,
    isPlay,
  });

const mapDispatchToProps = dispatch => ({
  onSelect: (trackId, isPlay, collectionId) => {
    dispatch(albumLookup(collectionId));
    dispatch(loadTrack(trackId, isPlay));
    dispatch(playerActive(true));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(TrackList);
