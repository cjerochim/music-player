import React from 'react';
import { shallow } from 'enzyme';
import configureStore from 'redux-mock-store';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import TrackListContainer from './TrackListContainer';
import uri from '../uri';

import {
  lookupAlbumSuccess,
} from '../redux/modules/album';

const mockStore = configureStore();

describe('<TrackListContainer />', () => {
  let wrapper;
  let store;
  beforeEach(() => {
    const state = {
      player: {
        isPlay: false,
        track: { trackId: 1234 },
        isBuffering: false,
      },
      library: { tracks: [] },
    };
    store = mockStore(state);
    store.dispatch = jest.fn();
    wrapper = shallow(<TrackListContainer store={store} />);
  });

  it('should render props correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });

  describe('onSelect', () => {
    beforeEach(() => {
      const mock = new MockAdapter(axios);
      const albumPath = uri.getAlbum(111111);
      const responseAlbum = { results: [{ track: 'Track 1' }, { track: 'Track 2' }] };
      mock.onGet(albumPath).reply(200, responseAlbum);
    });
    it('maps onSelect to dispatch albumLookup, loadTrack and playerActive actions', (done) => {
      const dispatch = jest.fn();
      const collectionId = 111111;
      const trackId = 55555;
      const isPlay = true;
      wrapper.props().onSelect(trackId, isPlay, collectionId);
      store.dispatch.mock.calls[0][0](dispatch)
        .then(() => {
          expect(dispatch).toHaveBeenCalledWith(lookupAlbumSuccess({ track: 'Track 1' }, [{ track: 'Track 2' }]));
          done();
        });
    });
  });
});
