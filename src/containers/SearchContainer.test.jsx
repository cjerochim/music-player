import React from 'react';
import { shallow } from 'enzyme';
import configureStore from 'redux-mock-store';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

import { searchTermUpdate } from '../redux/modules/search';
import SearchContainer from './SearchContainer';
import uri from '../uri';
import {
  lookupArtistStart,
  lookupArtistSuccess,
} from '../redux/modules/library';

const mockStore = configureStore();

describe('<SearchContainer />', () => {
  let wrapper;
  let store;
  beforeEach(() => {
    const state = { search: { searchTerm: 'Jack' } };
    store = mockStore(state);
    store.dispatch = jest.fn();
    wrapper = shallow(<SearchContainer store={store} />);
  });

  it('should render default props ', () => {
    expect(wrapper).toMatchSnapshot();
  });
  it('should map onUpdate to dispatch and update the searchTerm state', () => {
    const searchTerm = 'jack johnson';
    wrapper.props().onUpdate(searchTerm);
    const action = searchTermUpdate(searchTerm);
    expect(store.dispatch).toHaveBeenCalledWith(action);
  });
  describe('onSubmit', () => {
    beforeEach(() => {
      const mock = new MockAdapter(axios);
      const path = uri.getArtist('jack+johnson');
      const response = { resultCount: 10, results: [{ track: '1' }] };
      mock.onGet(path).reply(200, response);
    });
    it('should map onSubmit to dispatch and update the results state', (done) => {
      const dispatch = jest.fn();
      const searchTerm = 'jack+johnson';
      wrapper.props().onSubmit(searchTerm);
      store.dispatch.mock.calls[0][0](dispatch)
        .then(() => {
          expect(dispatch).toHaveBeenCalledWith(lookupArtistStart());
          expect(dispatch).toHaveBeenCalledWith(lookupArtistSuccess(10, [{ track: '1' }]));
          done();
        })
        .catch((err) => {
          expect(err).toBeUndefined();
        });
    });
  });
});
