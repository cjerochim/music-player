import React from 'react';
import { shallow } from 'enzyme';
import configureStore from 'redux-mock-store';
import RootContainer from './RootContainer';

const mockStore = configureStore();

describe('<RootContainer />', () => {
  let wrapper;
  let store;
  beforeEach(() => {
    store = mockStore({ });
    store.dispatch = jest.fn();
    wrapper = shallow(<RootContainer store={store} />);
  });
  it('should render default props', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
