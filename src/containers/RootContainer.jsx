import React from 'react';
import { Provider } from 'react-redux';
import createStore from '../redux/create';

import App from '../containers/AppContainer';

const store = createStore({});

export default () => (
  <Provider store={store}>
    <App />
  </Provider>
);
