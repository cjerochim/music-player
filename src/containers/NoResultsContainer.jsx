import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { refreshQuote } from '../redux/modules/quote';
import NoResults from '../components/NoResults/NoResults';

const mapStateToProps = ({
  player: { isActive },
  quote: { selected },
}) =>
  ({
    isAlbumActive: isActive,
    ...selected,
  });

const mapDispatchToProps = dispatch =>
  bindActionCreators({
    refreshQuote,
  }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(NoResults);
