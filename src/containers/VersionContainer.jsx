import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Version from '../components/version/Version';

const mapStateToProps = ({
  app: { version },
}) =>
  ({
    version,
  });

const mapDispatchToProps = dispatch =>
  bindActionCreators({
  }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Version);
