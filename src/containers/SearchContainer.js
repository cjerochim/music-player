import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Search from '../components/Search/Search';

import { libraryLookupArtist } from '../redux/modules/library';
import { searchTermUpdate } from '../redux/modules/search';

const mapStateToProps = ({
  search: { searchTerm },
}) =>
  ({
    searchTerm,
  });

const mapDispatchToProps = dispatch =>
  bindActionCreators({
    onSubmit: libraryLookupArtist,
    onUpdate: searchTermUpdate,
  }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Search);
