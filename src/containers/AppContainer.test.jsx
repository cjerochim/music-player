import React from 'react';
import { shallow } from 'enzyme';
import configureStore from 'redux-mock-store';
import AppContainer from './AppContainer';

const mockStore = configureStore();
describe('<AppContainer />', () => {
  let wrapper;
  let store;
  beforeEach(() => {
    const state = {
      player: { isActive: false },
    };
    store = mockStore(state);
    store.dispatch = jest.fn();
    wrapper = shallow(<AppContainer store={store} />);
  });

  it('renders with default props', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
