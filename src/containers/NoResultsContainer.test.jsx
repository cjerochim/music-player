import React from 'react';
import { shallow } from 'enzyme';
import configureStore from 'redux-mock-store';
import NoResultsContainer from './NoResultsContainer';

import { refreshQuote } from '../redux/modules/quote';

const mockStore = configureStore();

describe('<NoResultsContainer />', () => {
  let wrapper;
  let store;
  beforeEach(() => {
    const state = {
      player: {
        isActive: false,
      },
      quote: {
        quoteIndex: 0,
        collection: [
          { text: 'This is a cool quote', by: 'Someone' },
          { text: 'This is another cool quote', by: 'Someone else' },
          { text: 'This is another another cool quote', by: 'Someone else else' },
          { text: 'This is another another cool cool quote', by: 'Someone someone else else' },
        ],
      },
    };
    store = mockStore(state);
    store.dispatch = jest.fn();
    wrapper = shallow(<NoResultsContainer store={store} />);
  });

  it('renders with default props', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('should map the refreshQuote to dispatch and update the quote state', () => {
    wrapper.props().refreshQuote();
    const action = refreshQuote();
    expect(store.dispatch).toHaveBeenCalledWith(action);
  });
});
